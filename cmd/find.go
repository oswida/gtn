package cmd

import (
	"gtn/common"
	"path/filepath"

	"github.com/AlecAivazis/survey/v2"
	"github.com/desertbit/grumble"
	"github.com/pterm/pterm"
	"github.com/skratchdot/open-golang/open"
)

func CmdFind(env *common.CmdEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "find",
		Help:    "find note using ripgrep fulltext search, use double quotes if term contains spaces",
		Aliases: []string{"f"},

		Args: func(a *grumble.Args) {
			a.String("term", "term to find")
		},
		Flags: func(f *grumble.Flags) {
			f.Bool("s", "select", false, "if present, search results are shown as a selection list")
			f.Bool("e", "edit", false, "if present, selected search result is edited instead shown")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineRoot(c)
			term := c.Args.String("term")
			notes, err := common.FindNotesByTerm(env, term)
			if err != nil {
				pterm.Info.Println("Nothing found for: " + term)
				return nil
			}
			if c.Flags.Bool("select") {
				slist := common.NotePrintFromFiles(env.CurrentDir, notes)
				sel := ""
				prompt := &survey.Select{
					Message: "",
					Options: slist,
				}
				err = survey.AskOne(prompt, &sel)
				if err != nil {
					return err
				}
				if sel != "" {
					idx := -1
					for i, s := range slist {
						if s == sel {
							idx = i
							break
						}
					}
					if idx == -1 {
						return nil
					}
					note := notes[idx]
					if c.Flags.Bool("edit") {
						err = open.Run(filepath.Join(env.CurrentDir, note))
						if err != nil {
							return err
						}
						return nil
					}
					h, err := common.FmReadHeader(filepath.Join(env.CurrentDir, note))
					if err != nil {
						return err
					}
					err = common.PrintContent(env, h.Number)
					if err != nil {
						return err
					}
				}
			} else {
				err = common.PrintSearch(env, term, notes)
				if err != nil {
					return err
				}
			}
			return nil
		},
	}
}

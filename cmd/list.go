package cmd

import (
	"gtn/common"
	"strings"

	"github.com/desertbit/grumble"
)

func CmdList(env *common.CmdEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "list",
		Help:    "view categories list or note list for a selected category",
		Aliases: []string{"l"},

		Args: func(a *grumble.Args) {
			a.String("category", "category for note list", grumble.Default(""))
		},

		Flags: func(f *grumble.Flags) {
			f.Bool("a", "all", false, "show notes from all categories")
		},

		Completer: func(prefix string, args []string) []string {
			retv := []string{}
			lst, _ := common.DirList(env.CurrentDir)
			if strings.Trim(prefix, " \t\n\r") == "" {
				return lst
			}
			for _, l := range lst {
				if strings.HasPrefix(l, prefix) {
					retv = append(retv, l)
				}
			}
			return retv
		},

		Run: func(c *grumble.Context) error {
			env.DetermineRoot(c)
			if c.Flags.Bool("all") {
				return common.PrintAllNotes(env)
			}
			catname := c.Args.String("category")
			if catname == "" {
				err := common.PrintCategories(env)
				if err != nil {
					return err
				}
				return nil
			}
			err := common.PrintNotes(env, catname)
			if err != nil {
				return err
			}
			return nil
		},
	}
}

package cmd

import (
	"gtn/common"
	"path/filepath"

	"github.com/AlecAivazis/survey/v2"
	"github.com/desertbit/grumble"
)

func CmdEncrypt(env *common.CmdEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "encrypt",
		Help:    "encrypt note content",
		Aliases: []string{"c"},

		Args: func(a *grumble.Args) {
			a.Int("id", "note id")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineRoot(c)
			id := c.Args.Int("id")
			pth, err := common.FindNoteByNumber(env.CurrentDir, id)
			if err != nil {
				return err
			}
			pth = filepath.Join(env.CurrentDir, pth)
			content, header, err := common.FmReadNoteFile(pth)
			if err != nil {
				return err
			}
			var passwd string
			prompt := &survey.Password{
				Message: "Encrypt keyphrase",
			}
			err = survey.AskOne(prompt, &passwd)
			if err != nil {
				return err
			}
			if passwd != "" {
				pwd, err := common.DeriveKey(passwd)
				if err != nil {
					return err
				}
				content, err = common.Encrypt(common.UuidFromFilename(pth), content, pwd)
				if err != nil {
					return err
				}
				header.Encrypted = true
				return common.FmWriteNoteParts(pth, header, content)
			}
			return nil
		},
	}
}

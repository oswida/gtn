package cmd

import (
	"errors"
	"gtn/common"
	"path/filepath"

	"github.com/desertbit/grumble"
	"github.com/skratchdot/open-golang/open"
)

func CmdEdit(env *common.CmdEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "edit",
		Help:    "edit note content, note is determined by a given id",
		Aliases: []string{"e"},

		Args: func(a *grumble.Args) {
			a.Int("id", "note id")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineRoot(c)
			id := c.Args.Int("id")
			cnt := common.TotalFileCount(env.CurrentDir)
			if id < 1 || id > cnt {
				return errors.New("bad note id")
			}
			pth, err := common.FindNoteByNumber(env.CurrentDir, id)
			if err != nil {
				return err
			}
			err = open.Run(filepath.Join(env.CurrentDir, pth))
			if err != nil {
				return err
			}
			return nil
		},
	}
}

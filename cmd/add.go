package cmd

import (
	"gtn/common"
	"path/filepath"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/desertbit/grumble"
	"github.com/google/uuid"
	"github.com/skratchdot/open-golang/open"
)

func CmdAdd(env *common.CmdEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "add",
		Help:    "add new note to specified category",
		Aliases: []string{"a"},

		Args: func(a *grumble.Args) {
			a.String("category", "note category for a new note")
			a.String("title", "new note title", grumble.Default(""))
		},

		Completer: func(prefix string, args []string) []string {
			retv := []string{}
			lst, _ := common.DirList(env.CurrentDir)
			if strings.Trim(prefix, " \t\n\r") == "" {
				return lst
			}
			for _, l := range lst {
				if strings.HasPrefix(l, prefix) {
					retv = append(retv, l)
				}
			}
			return retv
		},

		Run: func(c *grumble.Context) error {
			env.DetermineRoot(c)
			catname := c.Args.String("category")
			err := common.CreateCat(catname, env)
			if err != nil {
				return err
			}
			uid := uuid.NewString()
			fname := uid + ".md"
			pth := filepath.Join(env.CurrentDir, catname, fname)
			title := c.Args.String("title")
			if title == "" {
				prompt := &survey.Input{
					Message: "Title: ",
				}
				err = survey.AskOne(prompt, &title)
				if err != nil {
					return err
				}
			}
			num := common.TotalFileCount(env.CurrentDir) + 1
			err = common.MakeFileIfNotExists(pth, []byte(common.FmNoteHeader(num, catname, title, false)))
			if err != nil {
				return err
			}
			err = open.Run(pth)
			if err != nil {
				return err
			}
			return nil
		},
	}
}

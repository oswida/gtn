package cmd

import (
	"gtn/common"

	"github.com/desertbit/grumble"
	"github.com/pterm/pterm"
)

var (
	apptitle = pterm.FgLightYellow.Sprint("Console notebook application.\n")
	appnote  = pterm.FgLightGreen.Sprint("IMPORTANT! For proper work, this program requires \"ripgrep\" software on executable path.")
)

func NewCmdEnv() *common.CmdEnv {
	retv := &common.CmdEnv{}
	app := grumble.New(&grumble.Config{
		Name: "gtn",
		Description: apptitle + `
Notes are stored in markdown files. 
The root directory for a notes is determined in a following order: 
	-r flag setting, 
	GTN_HOME env variable (if not empty), 
	current working dir.
Directory has a flat structure with only one level od subdirectories defining note categories.
Each note belongs to one category and is saved into a single file.
Notes are open with the system application assigned to Markdown files.
` + appnote,

		Flags: func(f *grumble.Flags) {
			f.String("r", "root", "", "notes root directory")
		},
	})
	retv.App = app
	registerCommands(retv)
	return retv
}

func registerCommands(env *common.CmdEnv) {
	env.App.AddCommand(CmdAdd(env))
	env.App.AddCommand(CmdList(env))
	env.App.AddCommand(CmdView(env))
	env.App.AddCommand(CmdEdit(env))
	env.App.AddCommand(CmdFind(env))
	env.App.AddCommand(CmdEncrypt(env))
}

package cmd

import (
	"errors"
	"gtn/common"

	"github.com/desertbit/grumble"
)

func CmdView(env *common.CmdEnv) *grumble.Command {
	return &grumble.Command{
		Name:    "view",
		Help:    "view note content, note is determined by a given id",
		Aliases: []string{"v"},

		Args: func(a *grumble.Args) {
			a.Int("id", "note id")
		},

		Run: func(c *grumble.Context) error {
			env.DetermineRoot(c)
			id := c.Args.Int("id")
			cnt := common.TotalFileCount(env.CurrentDir)
			if id < 1 || id > cnt {
				return errors.New("bad note id")
			}
			err := common.PrintContent(env, id)
			if err != nil {
				return err
			}
			return nil
		},
	}
}

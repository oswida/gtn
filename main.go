package main

import (
	"gtn/cmd"

	"github.com/pterm/pterm"
)

func main() {
	env := cmd.NewCmdEnv()
	err := env.App.Run()
	if err != nil {
		pterm.Error.WithShowLineNumber(false).Println(err.Error())
	}
}

package common

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func MakeFileIfNotExists(fpath string, initial []byte) error {
	if _, err := os.Stat(fpath); os.IsNotExist(err) {
		_, err = os.Create(fpath)
		if err != nil {
			return err
		}
		if len(initial) > 0 {
			err = ioutil.WriteFile(fpath, initial, os.ModePerm)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func MakeDirIfNotExists(fpath string) error {
	if _, err := os.Stat(fpath); os.IsNotExist(err) {
		err = os.MkdirAll(fpath, os.ModePerm)
		if err != nil {
			return err
		}
	}
	return nil
}

func DirList(fpath string) ([]string, error) {
	retv := []string{}
	lst, err := os.ReadDir(fpath)
	if err != nil {
		return nil, err
	}
	for _, entry := range lst {
		if entry.IsDir() && !strings.HasPrefix(entry.Name(), ".") {
			retv = append(retv, entry.Name())
		}
	}
	return retv, nil
}

func FileList(fpath string) ([]string, error) {
	retv := []string{}
	lst, err := os.ReadDir(fpath)
	if err != nil {
		return nil, err
	}
	for _, entry := range lst {
		if !entry.IsDir() && !strings.HasPrefix(entry.Name(), ".") {
			retv = append(retv, entry.Name())
		}
	}
	return retv, nil
}

func DirExists(fpath string) bool {
	if _, err := os.Stat(fpath); os.IsNotExist(err) {
		return false
	}
	return true
}

func TotalFileCount(fpath string) int {
	cnt := 0
	dirs, _ := DirList(fpath)
	for _, d := range dirs {
		files, _ := FileList(filepath.Join(fpath, d))
		cnt = cnt + len(files)
	}
	return cnt
}

func FindNoteByNumber(fpath string, num int) (string, error) {
	retv := ""
	rgpath, err := exec.LookPath("rg")
	if err != nil {
		return retv, err
	}
	cmd := exec.Command(rgpath, "number: "+fmt.Sprintf("%d", num))
	cmd.Dir = fpath
	data, err := cmd.Output()
	if err != nil {
		return retv, errors.New("cannot find note number " + fmt.Sprintf("%d", num))
	}
	parts := strings.Split(string(data), ":")
	retv = parts[0]
	return retv, nil
}

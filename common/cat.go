package common

import (
	"errors"
	"os"
	"path/filepath"
)

func CreateCat(name string, env *CmdEnv) error {
	return MakeDirIfNotExists(filepath.Join(env.CurrentDir, name))
}

func RenameCat(oldname string, newname string, env *CmdEnv) error {
	if !DirExists(filepath.Join(env.CurrentDir, oldname)) {
		return errors.New("directory " + oldname + " does not exist")
	}
	if DirExists(filepath.Join(env.CurrentDir, newname)) {
		return errors.New("directory " + newname + " already exists")
	}
	return os.Rename(filepath.Join(env.CurrentDir, oldname), filepath.Join(env.CurrentDir, newname))
}

func DeleteCat(name string, env *CmdEnv, force bool) error {
	if !DirExists(filepath.Join(env.CurrentDir, name)) {
		return errors.New("directory " + name + " does not exist")
	}
	return os.Remove(filepath.Join(env.CurrentDir, name))
}

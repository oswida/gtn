package common

import (
	"fmt"
	"path/filepath"

	"github.com/AlecAivazis/survey/v2"
	"github.com/charmbracelet/glamour"
	"github.com/pterm/pterm"
)

func PrintCategories(env *CmdEnv) error {
	fmt.Println()
	pterm.Info.Println("Categories")
	lst, err := DirList(env.CurrentDir)
	if err != nil {
		return err
	}
	for i, l := range lst {
		files, err := FileList(filepath.Join(env.CurrentDir, l))
		if err != nil {
			return err
		}
		fmt.Printf("%d. %s %s\n", i+1, l, pterm.FgGray.Sprintf("(%d)", len(files)))
	}
	fmt.Println()
	return nil
}

func PrintNotes(env *CmdEnv, category string) error {
	fmt.Println()
	pterm.Info.Println("Notes in category: " + category)
	pth := filepath.Join(env.CurrentDir, category)
	files, err := FileList(pth)
	if err != nil {
		return err
	}
	for _, f := range files {
		h, err := FmReadHeader(filepath.Join(env.CurrentDir, category, f))
		if err != nil {
			return err
		}
		fmt.Printf("%s %s %s\n", pterm.FgLightYellow.Sprintf("%d.", h.Number), h.Title, pterm.FgGray.Sprintf(h.Created))
	}
	fmt.Println()
	return nil
}

func PrintAllNotes(env *CmdEnv) error {
	fmt.Println()
	pterm.Info.Println("All notes")
	files := []string{}
	lst, err := DirList(env.CurrentDir)
	if err != nil {
		return err
	}
	for _, dir := range lst {
		pth := filepath.Join(env.CurrentDir, dir)
		fls, err := FileList(pth)
		if err != nil {
			return err
		}
		for _, f := range fls {
			files = append(files, filepath.Join(dir, f))
		}
	}

	for _, f := range files {
		h, err := FmReadHeader(filepath.Join(env.CurrentDir, f))
		if err != nil {
			return err
		}
		fmt.Printf("%s %s %s %s\n", pterm.FgLightYellow.Sprintf("%d.", h.Number),
			h.Title,
			pterm.FgBlue.Sprintf("(%s)", h.Category),
			pterm.FgGray.Sprintf(h.Created))
	}
	fmt.Println()
	return nil
}

func PrintContent(env *CmdEnv, number int) error {
	fmt.Println()
	file, err := FindNoteByNumber(env.CurrentDir, number)
	if err != nil {
		return err
	}
	content, header, err := FmReadNoteFile(filepath.Join(env.CurrentDir, file))
	if err != nil {
		return err
	}
	if header.Encrypted {
		var passwd string
		prompt := &survey.Password{
			Message: "Encrypt keyphrase",
		}
		err = survey.AskOne(prompt, &passwd)
		if err != nil {
			return err
		}
		if passwd != "" {
			pwd, err := DeriveKey(passwd)
			if err != nil {
				return err
			}
			content, err = Decrypt(UuidFromFilename(file), content, pwd)
			if err != nil {
				return err
			}
		}
	}
	pterm.Info.Println(header.Title)
	out, err := glamour.Render(content, "dark")
	if err != nil {
		return err
	}
	fmt.Print(out)
	fmt.Println()
	return nil
}

func NotePrintFromFiles(currdir string, notes []string) []string {
	retv := []string{}
	for _, n := range notes {
		h, _ := FmReadHeader(filepath.Join(currdir, n))
		retv = append(retv, fmt.Sprintf("%s %s %s",
			pterm.FgLightYellow.Sprintf("%d.", h.Number),
			h.Title, pterm.FgGray.Sprintf("(%s)", h.Category)))
	}
	return retv
}

func PrintSearch(env *CmdEnv, term string, notes []string) error {
	fmt.Println()
	pterm.Info.Printfln("Search results for: " + term)
	nn := NotePrintFromFiles(env.CurrentDir, notes)
	for _, n := range nn {
		fmt.Println(n)
	}
	fmt.Println()
	return nil
}

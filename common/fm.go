package common

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"time"

	"gopkg.in/yaml.v3"
)

type NoteHeader struct {
	Number    int
	Created   string
	Category  string
	Title     string
	Encrypted bool
}

const fmtpl = `+++
number: %d
title: %s
created: %s
category: %s
encrypted: %v
+++`

func FmNoteHeader(num int, category string, title string, encrypted bool) string {
	return fmt.Sprintf(fmtpl, num, title, time.Now().Format("2006-02-01 15:02"), category, encrypted)
}

func FmReadHeader(fpath string) (NoteHeader, error) {
	retv := NoteHeader{}
	data, err := ioutil.ReadFile(fpath)
	if err != nil {
		return retv, err
	}
	r, _ := regexp.Compile(`\+\+\+[^\+]*\+\+\+`)
	res := r.FindString(string(data))
	if strings.Trim(res, " \n\r") != "" {
		res = strings.ReplaceAll(res, "+++", "")
		err = yaml.Unmarshal([]byte(res), &retv)
		if err != nil {
			return retv, err
		}
	} else {
		return retv, errors.New("bad message header at " + fpath)
	}
	return retv, nil
}

func FmReadNoteFile(fpath string) (string, NoteHeader, error) {
	retv := ""
	header := NoteHeader{}
	data, err := ioutil.ReadFile(fpath)
	if err != nil {
		return retv, header, err
	}
	r, _ := regexp.Compile(`\+\+\+[^\+]*\+\+\+`)
	res := r.FindString(string(data))
	if strings.Trim(res, " \n\r") != "" {
		retv = strings.ReplaceAll(string(data), res, "")
		res = strings.ReplaceAll(res, "+++", "")
		err = yaml.Unmarshal([]byte(res), &header)
		if err != nil {
			return retv, header, err
		}
	}
	return retv, header, nil
}

func FmWriteNoteParts(fpath string, header NoteHeader, content string) error {
	h := FmNoteHeader(header.Number, header.Category, header.Title, header.Encrypted)
	data := []byte(h)
	data = append(data, []byte("\n")...)
	data = append(data, []byte(content)...)
	return ioutil.WriteFile(fpath, data, os.ModePerm)
}

package common

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"fmt"

	"golang.org/x/crypto/scrypt"
)

func NonceFromUuid(uuid string) []byte {
	fmt.Println("getting nonce for " + uuid)
	return []byte(uuid)[0:12]
}

func DeriveKey(password string) ([]byte, error) {
	salt := []byte("mnBPqeSJmcFRwqOk1FmdiftUP33y3i2L")

	key, err := scrypt.Key([]byte(password), salt, 32768, 8, 1, 32)
	if err != nil {
		return nil, err
	}

	return key, nil
}

func Encrypt(uuid string, content string, key []byte) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	nonce := NonceFromUuid(uuid)
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}
	ciphertext := aesgcm.Seal(nil, nonce, []byte(content), nil)
	return fmt.Sprintf("%x", ciphertext), nil
}

func Decrypt(uuid string, hexdata string, key []byte) (string, error) {
	ciphertext, _ := hex.DecodeString(hexdata)
	nonce := NonceFromUuid(uuid)
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}
	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return "", err
	}
	return string(plaintext), nil
}

package common

import (
	"fmt"
	"os/exec"
	"strings"
)

func FindNotesByTerm(env *CmdEnv, term string) ([]string, error) {
	retv := []string{}
	rgpath, err := exec.LookPath("rg")
	if err != nil {
		return retv, err
	}
	cmd := exec.Command(rgpath, "-S", "-l", term)
	cmd.Dir = env.CurrentDir
	data, err := cmd.Output()
	if err != nil {
		return retv, err
	}
	lines := strings.Split(fmt.Sprintf("%s", data), "\n")
	for _, l := range lines {
		if strings.Trim(l, "\n\r\t ") != "" {
			retv = append(retv, l)
		}
	}
	return retv, nil
}

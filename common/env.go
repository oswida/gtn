package common

import (
	"os"

	"github.com/desertbit/grumble"
)

type CmdEnv struct {
	App        *grumble.App
	CurrentDir string
}

func (e *CmdEnv) DetermineRoot(c *grumble.Context) {
	dir := c.Flags.String("root")
	_, err := os.Stat(dir)
	if dir != "" && err == nil {
		e.CurrentDir = dir
	} else if os.Getenv("GTN_HOME") != "" {
		e.CurrentDir = os.Getenv("GTN_HOME")
	} else {
		dir, _ = os.Getwd()
		e.CurrentDir = dir
	}
}

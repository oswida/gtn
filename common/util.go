package common

import (
	"path/filepath"
	"strings"
)

func ContainsStr(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func UuidFromFilename(fname string) string {
	return strings.ReplaceAll(filepath.Base(fname), ".md", "")
}

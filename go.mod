module gtn

go 1.16

require (
	github.com/AlecAivazis/survey/v2 v2.2.12
	github.com/GeertJohan/go.rice v1.0.0 // indirect
	github.com/alecthomas/kong-hcl v0.1.8-0.20190615233001-b21fea9723c8 // indirect
	github.com/charmbracelet/glamour v0.3.0 // indirect
	github.com/desertbit/grumble v1.1.1
	github.com/gernest/front v0.0.0-20210301115436-8a0b0a782d0a // indirect
	github.com/google/uuid v1.2.0
	github.com/gorilla/csrf v1.6.0 // indirect
	github.com/gorilla/handlers v1.4.1 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/logrusorgru/aurora v0.0.0-20191116043053-66b7ad493a23 // indirect
	github.com/pterm/pterm v0.12.22
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/spf13/cobra v1.1.3
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
